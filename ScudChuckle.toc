## Interface: 80205
## Title: ScudChuckle
## Author: Scudmarx
## Version: 1.2.2
## Notes: Occasionally laughs on killing an enemy.
## DefaultState: Enabled
## SavedVariablesPerCharacter: ScudChuckle_LaughChance, ScudChuckle_Jolliness, ScudChuckle_HulkMode, ScudChuckle_Crits

ScudChuckle.xml
ScudChuckle.lua