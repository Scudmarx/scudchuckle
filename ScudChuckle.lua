function ScudChuckle_MakeVariables()
	ScudChuckle_lastTime = 0
	ScudChuckle_LaughChance = ((ScudChuckle_LaughChance == nil) and 0.5) or ScudChuckle_LaughChance
	ScudChuckle_Jolliness = ((ScudChuckle_Jolliness == nil) and 0.5) or ScudChuckle_Jolliness
	ScudChuckle_HulkMode = ((ScudChuckle_HulkMode == nil) and true) or ScudChuckle_HulkMode
	ScudChuckle_Crits = ((ScudChuckle_Crits == nil) and true) or ScudChuckle_Crits
	ScudChuckle_CalculateLaughTimer()
end

function ScudChuckle_CalculateLaughTimer()
	ScudChuckle_MinTimeBetweenLaughs = 1 + (9 * (1 - ScudChuckle_LaughChance))
end

function ScudChuckle_OnLoad(frame)
	frame:RegisterEvent("ADDON_LOADED")
	
	frame.name = "ScudChuckle"
	local myname = frame:GetName()
	
	frame.okay = function (frame)
		ScudChuckle_LaughChance = _G[myname.."_SliderTalkative"]:GetValue()
		ScudChuckle_Jolliness = _G[myname.."_SliderJolliness"]:GetValue()
		ScudChuckle_HulkMode = _G[myname.."_HulkCheckbox"]:GetChecked()
		ScudChuckle_Crits = _G[myname.."_CritCheckbox"]:GetChecked()
		ScudChuckle_CalculateLaughTimer()
	end

	frame.default = function (frame)
		ScudChuckle_LaughChance = 0.5
		ScudChuckle_Jolliness = 0.5
		ScudChuckle_HulkMode = true
		ScudChuckle_Crits = true
		ScudChuckle_CalculateLaughTimer()
		_G[myname.."_SliderTalkative"]:SetValue(ScudChuckle_LaughChance)
		_G[myname.."_SliderJolliness"]:SetValue(ScudChuckle_Jolliness)
		_G[myname.."_HulkCheckbox"]:SetChecked(ScudChuckle_HulkMode)
		_G[myname.."_CritCheckbox"]:SetChecked(ScudChuckle_Crits)
	end
	
	--_G[myname.."_Text"]:SetText("Settings for: "..UnitName("player"))
	_G[myname.."_SliderTalkativeLow"]:SetText("Silent")
	_G[myname.."_SliderTalkativeHigh"]:SetText("Annoying")
	_G[myname.."_SliderTalkativeText"]:SetText("Talkativity")
	_G[myname.."_SliderJollinessLow"]:SetText("Angry")
	_G[myname.."_SliderJollinessHigh"]:SetText("Jolly")
	_G[myname.."_SliderJollinessText"]:SetText("Personality")
	_G[myname.."_HulkCheckboxText"]:SetText("Gets angrier when hurt")
	_G[myname.."_CritCheckboxText"]:SetText("Gets excited by criticals")
	
	InterfaceOptions_AddCategory(frame);
end

function ScudChuckle_OnEvent(event, frame, ...)
	if (event == "COMBAT_LOG_EVENT_UNFILTERED") then
		local info = {CombatLogGetCurrentEventInfo()}
		local playerGUID = UnitGUID("player")
		local timestamp, subevent, _, sourceGUID = unpack(info)
		
		if (subevent == "PARTY_KILL") then
			ScudChuckle_OnKill(unpack(info))
		end
		if (ScudChuckle_Crits) then
			if (subevent == "SWING_DAMAGE") then
				local critical = select(18, unpack(info))
				if critical and ScudChuckle_Crits and (sourceGUID == playerGUID) then
					ScudChuckle_OnSwingCrit(unpack(info))
				end
			elseif (subevent == "SPELL_DAMAGE") then
				local critical = select(21, unpack(info))
				if critical and ScudChuckle_Crits and (sourceGUID == playerGUID) then
					ScudChuckle_OnSpellCrit(unpack(info))
				end
			end
		end
		
	elseif ((event == "ADDON_LOADED") and (... == "ScudChuckle")) then
		ScudChuckle_MakeVariables()
		frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
		frame:UnregisterEvent("ADDON_LOADED")
	end
end

function ScudChuckle_OnSwingCrit(...)
	local timestamp = ...
	local LaughChance = ScudChuckle_LaughChance
	local Jolliness = ScudChuckle_Jolliness

	--local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand = select(12, ...)
		
	-- It's cooler when you smash hard with something big.
	local isoffhand = select(21, ...)
	local mainspeed, offspeed = UnitAttackSpeed("player")
	local speed
	if isoffhand then speed = offspeed
	else speed = mainspeed
	end
	LaughChance = LaughChance + ((1 - LaughChance) * (math.min(speed, 3) / 6))
		
	-- When riding vehicles, you generally slaughter lots of enemies.  That's still fun, but you need to dial it back a bit.
	if UnitInVehicle("player") then 
		LaughChance = LaughChance * 0.2
		Jolliness = Jolliness + ((1 - Jolliness) * 0.3)
	end

	LaughChance = LaughChance * 0.2
	if (LaughChance > math.random()) then
		if (ScudChuckle_HulkMode) then Jolliness = Jolliness * (UnitHealth("player") / UnitHealthMax("player")) end
		ScudChuckle_DoEmote(timestamp, Jolliness)
	end
end

function ScudChuckle_OnSpellCrit(...)
	local timestamp = ...
	local LaughChance = ScudChuckle_LaughChance
	local Jolliness = ScudChuckle_Jolliness

	--local spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand = select(12, ...)
	local spellId = select(12, ...)

	-- Bigger spells are more exciting.
	local _, _, _, casttime = GetSpellInfo(spellId)
	LaughChance = LaughChance + ((1 - LaughChance) * (math.min(casttime, 5) / 8))

	-- When riding vehicles, you generally slaughter lots of enemies.  That's still fun, but you need to dial it back a bit.
	if UnitInVehicle("player") then 
		LaughChance = LaughChance * 0.2
		Jolliness = Jolliness + ((1 - Jolliness) * 0.3)
	end
	
	LaughChance = LaughChance * 0.2
	if (LaughChance > math.random()) then
		if (ScudChuckle_HulkMode) then Jolliness = Jolliness * (UnitHealth("player") / UnitHealthMax("player")) end
		ScudChuckle_DoEmote(timestamp, Jolliness)
	end
end

function ScudChuckle_OnKill(...)
	local timestamp, _, _, sourceGUID, _, _, _, killedGUID = ...
	local LaughChance = ScudChuckle_LaughChance
	local Jolliness = ScudChuckle_Jolliness
	
	-- When another party member kills something, can still laugh, but lower chance.
	if (sourceGUID == UnitGUID("player")) then 
		-- ok.
	elseif (sourceGUID == UnitGUID("pet")) then
		LaughChance = LaughChance * 0.6
	else
		LaughChance = LaughChance * 0.15
	end
	
	-- It's nicer to kill powerful foes.
	local deadClass = UnitClassification(killedGUID)
	if deadClass == "worldboss" then
		LaughChance = LaughChance * 3.0
	elseif deadClass == "rareelite" then
		LaughChance = LaughChance * 2.5
	elseif deadClass == "elite" then
		LaughChance = LaughChance * 2.0
	elseif deadClass == "rare" then
		LaughChance = LaughChance * 1.5
	elseif deadClass == "trivial" then
		LaughChance = LaughChance * 0.2
	elseif deadClass == "minus" then
		LaughChance = LaughChance * 0.35
	end
	
	-- When riding vehicles, you generally slaughter lots of enemies.  That's still fun, but you need to dial it back a bit.
	if UnitInVehicle("player") then 
		LaughChance = LaughChance * 0.2
		Jolliness = Jolliness + ((1 - Jolliness) * 0.3)
	end
	
	LaughChance = LaughChance * 0.5
	if (LaughChance > math.random()) then
		if (ScudChuckle_HulkMode) then Jolliness = Jolliness * (UnitHealth("player") / UnitHealthMax("player")) end
		ScudChuckle_DoEmote(timestamp, Jolliness)
	end
end

function ScudChuckle_DoEmote(timestamp, jolliness)
	if ((timestamp - ScudChuckle_lastTime) >= ScudChuckle_MinTimeBetweenLaughs) then
		ScudChuckle_lastTime = timestamp
		if (jolliness > math.random()) then ScudChuckle_BeHappy()
		else ScudChuckle_BeAngry()
		end
	end
end

function ScudChuckle_BeHappy()
	local emote = "chuckle"
	local emoteChoice = math.random()
	if (0.2 > emoteChoice) then emote = "laugh"
	end
	DoEmote(emote, "none")
end

function ScudChuckle_BeAngry()
	local emote = "roar"
	local emoteChoice = math.random()
	if (0.2 > emoteChoice) then emote = "cheer"
	end
	DoEmote(emote, "none")
end